<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserGenerator
{

    private EntityManagerInterface $entityManager;
    private UserPasswordHasherInterface $hasher;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordHasherInterface $hasher)
    {
        $this->entityManager = $entityManager;
        $this->hasher = $hasher;
    }

    /**
     * Create new user with security specifications
     * Return null if no problem has detected
     *
     * @param mixed $formData
     * @param FormInterface $interface
     * @return string[]|null
     */
    function createNewUser(mixed $formData, FormInterface $interface) : array|null {

        //
        // Min username length : 6
        if (strlen($formData->getUsername()) < 6) {
            return ['error', 'Le nom d\'utilisateur doit comporter au moins 6 caractères '];
        } else if (preg_match('/[^a-zA-Z\s]/', $formData->getUsername()) === 1) {
            return['error', 'Le nom d\'utilisateur ne peut pas compter de caractères spéciaux ou de chiffres. '];
        }

        //
        // Min password length : 8
        if (strlen($formData->getPassword()) < 8) {
            return['error', 'Le mot de passe doit comporter au moins 8 caractères '];
        } else if (preg_match('/[^a-zA-Z\s]/', $formData->getPassword()) !== 1) {
            //
            // Special character and number required
            return['error', 'Le mot de passe doit comporter au moins un chiffre et un caractère spécial '];
        } else if ($formData->getPassword() != $interface->getData()) {
            //
            // password & repeat password are not equals
            return['error', 'Les mots de passe ne correspondent pas !'];
        }

        //
        // Invalid email
        if (!filter_var($formData->getEmail(), FILTER_VALIDATE_EMAIL)) {
            return['error', 'L\'adresse mail n\'est pas valide !'];
        }

        //
        // Check if username or email is already used
        $userFound = $this->entityManager->getRepository(User::class)
            ->createQueryBuilder('u')
            ->orWhere('u.username = :username')
            ->orWhere('u.email = :email')
            ->setParameters([
                'username' => $formData->getUsername(),
                'email' => $formData->getEmail()
            ])->getQuery()->execute();

        //
        // Already used
        if (!empty($userFound)) {
            return['error', 'L\'adresse mail ou le nom d\'utilisateur est déjà pris.'];
        }

        //
        // Save
        $user = new User();
        $user->setUsername($formData->getUsername());
        $user->setEmail($formData->getEmail());
        $user->setPassword($this->hasher->hashPassword($user, $formData->getPassword()));
        $user->setRoles(['APP_USER']);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        //
        // No problem : return null
        return null;
    }

}