<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        //
        // Users test registration
        $userAdmin = new User();
        $userAdmin->setUsername('administrator');
        $userAdmin->setRoles([
            'APP_USER',
            'ROLE_ADMIN'
        ]);
        $userAdmin->setEmail('admin@demo.com');
        $userAdmin->setPassword($this->hasher->hashPassword($userAdmin, 'HelloAdmin1&'));
        $manager->persist($userAdmin);

        $userAdmin2 = new User();
        $userAdmin2->setUsername('administrator_2');
        $userAdmin2->setRoles([
            'APP_USER',
            'ROLE_ADMIN'
        ]);
        $userAdmin2->setEmail('admin2@demo.com');
        $userAdmin2->setPassword($this->hasher->hashPassword($userAdmin2, 'HelloAdmin2&'));
        $manager->persist($userAdmin2);

        $userApp = new User();
        $userApp->setUsername('stduser');
        $userApp->setRoles([
            'APP_USER'
        ]);
        $userApp->setEmail('user@demo.com');
        $userApp->setPassword($this->hasher->hashPassword($userApp, 'HelloUser1&'));
        $manager->persist($userApp);

        $userApp2 = new User();
        $userApp2->setUsername('stduser_2');
        $userApp2->setRoles([
            'APP_USER'
        ]);
        $userApp2->setEmail('user2@demo.com');
        $userApp2->setPassword($this->hasher->hashPassword($userApp2, 'HelloUser2&'));
        $manager->persist($userApp2);

        $categoryMaths = new Category();
        $categoryMaths->setName("Mathématiques");
        $manager->persist($categoryMaths);

        $categoryCrypto = new Category();
        $categoryCrypto->setName("Cryptologie");
        $manager->persist($categoryCrypto);

        $categoryAlan = new Category();
        $categoryAlan->setName('Biographie');
        $manager->persist($categoryAlan);

        $articleMaths1Now = new \DateTime();
        $articleMaths1 = new Article();
        $articleMaths1->setTitle('Texte en lorem ipsum');
        $articleMaths1->setCategory($categoryMaths);
        $articleMaths1->setAuthor($userAdmin);
        $articleMaths1->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sapien pellentesque habitant morbi tristique senectus et netus et malesuada. Malesuada proin libero nunc consequat interdum varius sit. Auctor elit sed vulputate mi sit amet mauris commodo. Elit at imperdiet dui accumsan sit amet. Et pharetra pharetra massa massa ultricies mi quis hendrerit dolor. Quam lacus suspendisse faucibus interdum posuere lorem. Adipiscing elit duis tristique sollicitudin nibh sit amet. Amet commodo nulla facilisi nullam vehicula ipsum. Neque vitae tempus quam pellentesque. Risus at ultrices mi tempus. Viverra nibh cras pulvinar mattis nunc sed blandit. Dignissim sodales ut eu sem integer vitae justo eget. Diam sollicitudin tempor id eu. Tellus in hac habitasse platea. Vulputate eu scelerisque felis imperdiet. Integer feugiat scelerisque varius morbi enim nunc faucibus a pellentesque. Eu turpis egestas pretium aenean pharetra magna.
        Enim nec dui nunc mattis enim ut tellus elementum. Magnis dis parturient montes nascetur ridiculus mus mauris vitae. Pretium nibh ipsum consequat nisl vel. Mi bibendum neque egestas congue quisque egestas. Velit sed ullamcorper morbi tincidunt ornare massa eget egestas purus. Dolor sit amet consectetur adipiscing elit ut aliquam purus. Lectus nulla at volutpat diam ut venenatis tellus in. Pulvinar etiam non quam lacus suspendisse. Laoreet non curabitur gravida arcu. Duis convallis convallis tellus id interdum velit laoreet. Sit amet mattis vulputate enim nulla aliquet porttitor. Massa ultricies mi quis hendrerit dolor magna. Turpis egestas pretium aenean pharetra magna ac placerat vestibulum. Aliquet sagittis id consectetur purus ut. Et malesuada fames ac turpis.');
        $articleMaths1->setIcon(null);
        $articleMaths1->setPublicationTime($articleMaths1Now->sub(new \DateInterval('P1D')));
        $manager->persist($articleMaths1);

        $articleMaths2Now = new \DateTime();
        $articleMaths2 = new Article();
        $articleMaths2->setTitle('Travaux universitaires');
        $articleMaths2->setCategory($categoryMaths);
        $articleMaths2->setAuthor($userAdmin);
        $articleMaths2->setContent('Turing passe la plus grande partie de 1937 et de 1938 à travailler sur divers sujets à l\'université de Princeton, sous la direction du logicien Alonzo Church qui a déjà encadré les travaux de Stephen Kleene sur la récursivité et de John Rosser sur le lambda-calcul. Il obtient en mai 1938 son Ph. D.19 de l\'université de Princeton ; son manuscrit présente la notion d\'hypercalcul, où les machines de Turing sont complétées par ce qu\'il appelle des oraclesc, autorisant ainsi l\'étude de problèmes qui ne peuvent pas être résolus de manière algorithmique. Church emploie pour la première fois l\'expression « machine de Turing » dans le compte rendu de la thèse de son élève dans le Journal of Symbolic Logic.
        (Wikipedia : https://fr.wikipedia.org/wiki/Alan_Turing - Section "Études supérieures et travaux sur la calculabilité").');
        $articleMaths2->setIcon('math-1547018_1280.jpg');
        $articleMaths2->setPublicationTime($articleMaths2Now->add(new \DateInterval('P1D')));
        $manager->persist($articleMaths2);

        $articleMaths3Now = new \DateTime();
        $articleMaths3 = new Article();
        $articleMaths3->setTitle('Résultats Lambda');
        $articleMaths3->setCategory($categoryMaths);
        $articleMaths3->setAuthor($userAdmin2);
        $articleMaths3->setContent('Turing obtient des résultats importants sur le lambda-calcul, notamment en montrant son équivalence avec son propre modèle de calculabilité20, en inventant le combinateur de point-fixe qui porte son nom21 et en proposant la première démonstration de la normalisation du lambda calcul typé22.
        (Wikipedia : https://fr.wikipedia.org/wiki/Alan_Turing - Section "Études supérieures et travaux sur la calculabilité").');
        $articleMaths3->setIcon('calculator-988017_1280.jpg');
        $articleMaths3->setPublicationTime($articleMaths3Now->sub(new \DateInterval('P7D')));
        $manager->persist($articleMaths3);

        $articleCrypto1Now = new \DateTime();
        $articleCrypto1 = new Article();
        $articleCrypto1->setTitle('Cryptanalyse');
        $articleCrypto1->setCategory($categoryCrypto);
        $articleCrypto1->setAuthor($userAdmin);
        $articleCrypto1->setContent('Selon plusieurs historiens, le travail de Turing pour déchiffrer le code des transmissions allemandes permit de raccourcir la Seconde Guerre mondiale de deux ans23.
        (Wikipedia : https://fr.wikipedia.org/wiki/Alan_Turing - Section "Cryptanalyse").');
        $articleCrypto1->setIcon(null);
        $articleCrypto1->setPublicationTime($articleCrypto1Now->sub(new \DateInterval('P1D')));
        $manager->persist($articleCrypto1);

        $articleCrypto2Now = new \DateTime();
        $articleCrypto2 = new Article();
        $articleCrypto2->setTitle('Cryptanalyse : historique');
        $articleCrypto2->setCategory($categoryCrypto);
        $articleCrypto2->setAuthor($userAdmin2);
        $articleCrypto2->setContent('Fin 1938, après les accords de Munich, la Grande-Bretagne comprend le danger du nazisme et développe ses armements. Turing fait partie des jeunes cerveaux appelés à suivre des cours de chiffre et de cryptanalyse à la Government Code and Cypher School (GC&CS). Juste avant la déclaration de guerre, il rejoint le centre secret de la GC&CS à Bletchley Park.
        (Wikipedia : https://fr.wikipedia.org/wiki/Alan_Turing - Section "Cryptanalyse").');
        $articleCrypto2->setIcon(null);
        $articleCrypto2->setPublicationTime($articleCrypto2Now->sub(new \DateInterval('P1D')));
        $manager->persist($articleCrypto2);

        $articleCrypto3Now = new \DateTime();
        $articleCrypto3 = new Article();
        $articleCrypto3->setTitle('Cryptanalyse : ');
        $articleCrypto3->setCategory($categoryCrypto);
        $articleCrypto3->setAuthor($userAdmin);
        $articleCrypto3->setContent('Jusqu\'au milieu des années 1970, seuls quelques anciens cryptanalystes français25 et polonais26 avaient publié quelques informations sur la lutte contre Enigma dans leurs pays respectifs ; les capacités de décryptage de Bletchley Park et l\'opération Ultra restaient un secret militaire absolu en Grande-Bretagne. Puis les autorités britanniques déclassifièrent progressivement les techniques de décryptage d\'Enigma jusqu\'à 2000.
        (Wikipedia : https://fr.wikipedia.org/wiki/Alan_Turing - Section "Cryptanalyse").');
        $articleCrypto3->setIcon(null);
        $articleCrypto3->setPublicationTime($articleCrypto3Now->add(new \DateInterval('P2D')));
        $manager->persist($articleCrypto3);

        $articleBio1Now = new \DateTime();
        $articleBio1 = new Article();
        $articleBio1->setTitle('Biographie #1');
        $articleBio1->setCategory($categoryCrypto);
        $articleBio1->setAuthor($userAdmin2);
        $articleBio1->setContent('À partir de l\'âge d\'un an, le jeune Alan est élevé en Grande-Bretagne par des amis de la famille Turing, car sa mère a rejoint son père qui était en fonction dans l’Indian Civil Service. Ils reviennent au Royaume-Uni à la retraite de Julius en 1926
        (Wikipedia : https://fr.wikipedia.org/wiki/Alan_Turing - Section "Biographie").');
        $articleBio1->setIcon('alan_turin-923015_1280.jpeg');
        $articleBio1->setPublicationTime($articleBio1Now->sub(new \DateInterval('P1D')));
        $manager->persist($articleBio1);

        $articleBio2Now = new \DateTime();
        $articleBio2 = new Article();
        $articleBio2->setTitle('Biographie #2');
        $articleBio2->setCategory($categoryCrypto);
        $articleBio2->setAuthor($userAdmin);
        $articleBio2->setContent('Mais tout ce travail doit forcément rester secret, et ne sera connu du public que dans les années 1970. Après la guerre, il travaille sur un des tout premiers ordinateurs, puis contribue au débat sur la possibilité de l\'intelligence artificielle, en proposant le test de Turing.
        (Wikipedia : https://fr.wikipedia.org/wiki/Alan_Turing - Section "Biographie").');
        $articleBio2->setIcon('alan_turin-923015_1280.jpeg');
        $articleBio2->setPublicationTime($articleBio2Now->sub(new \DateInterval('P1D')));
        $manager->persist($articleBio2);

        $articleBio3Now = new \DateTime();
        $articleBio3 = new Article();
        $articleBio3->setTitle('Biographie #3');
        $articleBio3->setCategory($categoryCrypto);
        $articleBio3->setAuthor($userAdmin2);
        $articleBio3->setContent('Poursuivi en justice en 1952 pour homosexualité, il choisit, pour éviter la prison, la castration chimique par prise d\'œstrogènes. Il est retrouvé mort par empoisonnement au cyanure le 8 juin 1954 dans la chambre de sa maison à Wilmslow.
        (Wikipedia : https://fr.wikipedia.org/wiki/Alan_Turing - Section "Biographie").');
        $articleBio3->setIcon(null);
        $articleBio3->setPublicationTime($articleBio3Now->sub(new \DateInterval('P1D')));
        $manager->persist($articleBio3);

        $manager->flush();
    }
}
