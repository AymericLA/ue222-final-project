<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        //
        // Registration form
        // - username
        // - password
        // - repeat password field (not mapped)
        // - email

        $builder
            ->add('username', TextType::class, [
                'required' => true,
                'attr' => [
                    'minlength' => 6
                ],
                'label' => 'Nom d\'utilisateur'
            ])
            ->add('password', PasswordType::class, [
                'required' => true,
                'label' => 'Mot de passe',
                'attr' => [
                    'minlength' => 8,
                ]
            ])
            ->add('repeatPassword', PasswordType::class, [
                'label' => 'Répétez le mot de passe',
                'attr' => [
                    'minlength' => 8,
                ],
                'required' => true,
                'mapped' => false
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'Adresse mail'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'S\'inscrire'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
