<?php

namespace App\Form;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        //
        // Search navigation form :
        // - text field (not mapped)

        $builder
            ->add('searchItem', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'type' => 'search',
                    'placeholder' => 'Rechercher...',
                    'aria-label' => 'Search'
                ],
                'mapped' => false,
                'required' => true,
                'label' => false
            ])
            ->add('submit', SubmitType::class, [
                'attr' => [
                    'class' => 'btn',
                    'type' => 'submit'
                ],
                'label' => 'Rechercher'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
