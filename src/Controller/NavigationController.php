<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Form\SearchType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NavigationController extends AbstractController
{

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Api bridge to search articles from navigation menu form
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/api/search', name:'__api_search__')]
    public function navigationSearch(Request $request) : Response {
        //
        // Make form
        $form = $this->createForm(SearchType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //
            // Form requested and valid
            $data = $form->get('searchItem')->getData();

            //
            // Search published articles, where :
            // --> the title contains partially or totally word
            // --> the content contains partially or totally word
            // --> the category name contains partially or totally word

            $query = $this->entityManager->getRepository(Article::class)->createQueryBuilder('a')
                ->orWhere('a.title LIKE :searchRequest')
                ->orWhere('a.content LIKE :searchRequest')
                ->innerJoin(Category::class, 'c')
                ->orWhere('c.name LIKE :searchRequest')
                ->setParameter('searchRequest', '%'.$data.'%')
                ->andWhere('a.publicationTime <= CURRENT_DATE()')
                ->getQuery()
                ->getResult();

            //
            // Show articles
            return $this->render('article/results.html.twig', [
                'articles' => $query
            ]);
        }

        //
        // Default HTML element : navigation form
        return $this->render('elements/menuSearch.html.twig', [
            'form' => $form->createView()
        ]);
    }

}