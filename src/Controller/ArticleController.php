<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/article')]
class ArticleController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * List of published articles to be read by users
     * @return Response
     */
    #[Route('/', name: 'article_index', methods: ['GET'])]
    public function index(): Response
    {
        //
        // Get all articles where the publication date is before or equal to now
        $publishedArticles = $this->entityManager->getRepository(Article::class)->createQueryBuilder('a')
            ->where('a.publicationTime <= CURRENT_DATE()')
            ->getQuery()
            ->getResult();

        return $this->render('article/index.html.twig', [
            'articles' => $publishedArticles,
        ]);
    }

    /**
     * Read article by ID
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/{id}', name: 'article_show', methods: ['GET'])]
    public function show(Request $request): Response
    {
        if (!$request->get('id')) {
            //
            // ID not in request body
            $this->addFlash('error', 'Une erreur est survenue lors de votre demande');
            return $this->redirectToRoute('article_index');
        }

        //
        // Search article by ID
        $article = $this->entityManager->getRepository(Article::class)->find($request->get('id'));

        if (!$article) {
            //
            // Article not found
            $this->addFlash('error', 'L\'article n\'existe pas !');
            return $this->redirectToRoute('article_index');
        }

        //
        // Check if article is published
        $now = new \DateTime();
        if ($now->format('Y-m-d') < $article->getPublicationTime()->format('Y-m-d')) {
            $this->addFlash('error', 'L\'article n\'est pas encore publié !');
            return $this->redirectToRoute('article_index');
        }

        //
        // Render article page
        return $this->render('article/show.html.twig', [
            'uploadDir' => $this->getParameter('upload_directory'),
            'article' => $article,
        ]);
    }

    public function getLastArticlesForNavigation(EntityManagerInterface  $entityManager) {
        $articles = $entityManager->getRepository(Article::class)->getLastArticlesForMenu();

        return $this->render('elements/menuArticles.html.twig', [
            'articles' => $articles
        ]);
    }
}
