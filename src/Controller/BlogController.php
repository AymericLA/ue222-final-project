<?php

namespace App\Controller;

use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Blog home page : render 5 last published articles
     *
     * @return Response
     */
    #[Route('/', name: 'blog')]
    public function index(): Response
    {
        //
        // Get 5 last published articles
        $lastArticles = $this->entityManager->getRepository(Article::class)->createQueryBuilder('a')
            ->orderBy('a.publicationTime', 'ASC')
            ->where('a.publicationTime <= CURRENT_DATE()')
            ->setMaxResults(5)->getQuery()->getResult();

        //
        // Render blog home page
        return $this->render('blog/index.html.twig', [
            'uploadDir' => $this->getParameter('upload_directory'),
            'lastArticles' => $lastArticles
        ]);
    }
}
