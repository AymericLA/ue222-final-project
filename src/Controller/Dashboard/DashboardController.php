<?php

namespace App\Controller\Dashboard;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/dashboard')]
class DashboardController extends AbstractController
{

    /**
     *
     * Only dashboard home view
     *
     * @return Response
     */
    #[Route('/', name: 'dashboard_home')]
    public function home() : Response {
        return $this->render('dashboard/home.html.twig');
    }

}