<?php

namespace App\Controller\Dashboard;

use App\Entity\User;
use App\Form\UserEditType;
use App\Service\UserGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/dashboard/users')]
class UserManagementController extends AbstractController {
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * List of users
     *
     * @return Response
     */
    #[Route('/list', name : 'dashboard_users_list')]
    public function usersList() : Response {
        $users = $this->entityManager->getRepository(User::class)->findAll();

        //
        // Set table header with ID, username, email, role and actions
        $tableHeader = [
            'N° identification',
            'Nom d\'utilisateur',
            'Email',
            'Type',
            'Actions'
        ];

        $table = [];

        foreach ($users as $user) {
            //
            // Set showed role & actions
            $role = 'Utilisateur';
            $actions = [
                'Editer' => [
                    'class' => 'btn-primary',
                    'url' => 'dashboard_users_edit',
                    'urlParameters' => [
                        'id' => $user->getId()
                    ]
                ],
            ];

            if (array_search('ROLE_ADMIN', $user->getRoles())) {
                //
                // Roles contains ROLE_ADMIN
                $role = 'Administrateur';
            } else {
                //
                // The APP_USERS can be deleted by administrator
                $actions = [
                    'Editer' => [
                        'class' => 'btn-primary',
                        'url' => 'dashboard_users_edit',
                        'urlParameters' => [
                            'id' => $user->getId()
                        ]
                    ],
                    'Supprimer' => [
                        'class' => 'btn-danger',
                        'url' => 'dashboard_users_remove',
                        'urlParameters' => [
                            'id' => $user->getId()
                        ]
                    ]
                ];
            }

            //
            // Add table entry
            $table[] = [
                $user->getId(),
                $user->getUserIdentifier(),
                $user->getEmail(),
                $role,
                $actions
            ];
        }

        //
        // Make extra button
        $extra = [
            'Créer un utilisateur' => [
                'class' => 'btn-info',
                'url' => 'dashboard_users_new'
            ]
        ];

        //
        // Render list layout
        return $this->render('dashboard/list.html.twig', [
            'table' => $table,
            'tableHeader' => $tableHeader,
            'extra' => $extra,
            'title' => 'Liste des utilisateurs'
        ]);
    }

    /**
     *  Add new user by email, username, password and role
     *
     * @param Request $request
     * @param UserPasswordHasherInterface $hasher
     * @return Response
     */
    #[Route('/new', name : 'dashboard_users_new')]
    public function addUser(Request $request, UserPasswordHasherInterface $hasher) : Response {
        //
        // Make form
        $form = $this->createForm(UserEditType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //
            // Form requested & checked : call UserGenerator service to register user
            $userGenerator = new UserGenerator($this->entityManager, $hasher);
            $generationStatus = $userGenerator->createNewUser($form->getData(), $form->get('repeatPassword'));

            if ($generationStatus != null) {
                //
                // User creation error (error msg returned by string) : add alert with returned message
                $this->addFlash($generationStatus[0], $generationStatus[1]);
            } else {
                //
                // Get user in DB to add ADMIN role if is defined in form
                $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $form->getData()->getUsername()]);
                if (!$user) {
                    $this->addFlash('error', 'Il y a eu une erreur dans la création de l\'utilisateur. ');
                } else {
                    if ($form->get('roles')->getData() == 'ROLE_ADMIN') {
                        $user->setRoles(['APP_USER', 'ROLE_ADMIN']);

                        //
                        // Save
                        $this->entityManager->persist($user);
                        $this->entityManager->flush();
                    }

                    $this->addFlash('success', 'L\'utilisateur a bien été sauvergardé. ');
                }
            }

        }

        //
        // Render form layout
        return $this->render('dashboard/form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Ajouter un utilisateur'
        ]);
    }

    /**
     * Edit user data
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/edit/{id}', name : 'dashboard_users_edit')]
    public function editUser(Request $request) : Response {
        $id = $request->get('id');

        if (empty($id)) {
            //
            // Not ID requested
            $this->addFlash('error', 'L\'ID de l\'utilisateur est manquant.');
            return $this->redirectToRoute('dashboard_users_list');
        }

        //
        // Search user in DB
        $user = $this->entityManager->getRepository(User::class)->find($id);
        if (!$user) {
            //
            // User not found
            $this->addFlash('error', 'L\'utilisateur est introuvable.');
            return $this->redirectToRoute('dashboard_users_list');
        }

        //
        // Make form
        $form = $this->createForm(UserEditType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //
            // Form requested & validated : set roles
            if ($form->get('roles')->getData() == 'APP_USER') {
                $user->setRoles(['APP_USER']);
            } else {
                $user->setRoles(['APP_USER', 'ROLE_ADMIN']);
            }

            //
            // Save
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $this->addFlash('success', 'L\'utilisateur a bien été modifié.');

            //
            // Go to users lis layout
            return $this->redirectToRoute('dashboard_users_list');
        }

        //
        // Render form layout
        return $this->render('dashboard/form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Modifier un utilisateur'
        ]);
    }

    /**
     * Delete user, with articles where is authored
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/remove/{id}', name : 'dashboard_users_remove')]
    public function removeUser(Request $request) : Response {

        //
        // Check if ID is in request body
        if (!empty($request->get('id'))) {
            $user = $this->entityManager->getRepository(User::class)->find($request->get('id'));
            if ($user) {
                //
                // User found : delete
                $this->entityManager->remove($user);
                $this->entityManager->flush();
                $this->addFlash('success', 'L\'utilisateur ' . $user->getUserIdentifier() . ' a bien été supprimé.');
            } else {
                //
                // Not found
                $this->addFlash('error', 'L\'utilisateur est introuvable.');
            }
        }

        return $this->redirectToRoute('dashboard_users_list');
    }

}