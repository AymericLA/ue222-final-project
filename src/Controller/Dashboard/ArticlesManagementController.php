<?php

namespace App\Controller\Dashboard;

use App\Entity\Article;
use App\Form\ArticleType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route('/dashboard/articles')]
class ArticlesManagementController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * List of all articles saved in DB
     * @return Response
     */
    #[Route('/list', name: 'dashboard_articles_list')]
    public function articlesLists() : Response
    {
        $articles = $this->entityManager->getRepository(Article::class)->findAll();

        //
        // Define the results table header
        $tableHeader = [
            'N° identification',
            'Titre',
            'Résumé',
            'Auteur',
            'Actions'
        ];

        $table = [];

        //
        // Each articles to set the summary & author name and actions
        foreach ($articles as $article) {
            $content = $article->getContent();

            //
            // If the length is greater 25, cut text and add "..."
            if (strlen($content) > 25) $content = substr($content, 0, 25) . '...';

            //
            // Set table entry with data
            $table[] = [
                $article->getId(),
                $article->getTitle(),
                $content,
                $article->getAuthor()->getUserIdentifier(),
                [
                    'Lire' => [
                        'class' => 'btn-info',
                        'url' => 'article_show',
                        'urlParameters' => [
                            'id' => $article->getId()
                        ]
                    ],
                    'Editer' => [
                        'class' => 'btn-primary',
                        'url' => 'dashboard_articles_edit',
                        'urlParameters' => [
                            'id' => $article->getId()
                        ]
                    ],
                    'Supprimer' => [
                        'class' => 'btn-danger',
                        'url' => 'dashboard_articles_delete',
                        'urlParameters' => [
                            'id' => $article->getId()
                        ]
                    ]
                ]
            ];
        }

        //
        // Extra action
        $extra = [
            'Nouvel article' => [
                'class' => 'btn-info',
                'url' => 'dashboard_articles_news'
            ]
        ];

        //
        // Render the list layout
        return $this->render('dashboard/list.html.twig', [
            'table' => $table,
            'tableHeader' => $tableHeader,
            'extra' => $extra,
            'title' => 'Liste des articles'
        ]);
    }

    /**
     * View to write new article
     * @param Request $request
     * @return Response
     */
    #[Route('/new', name: 'dashboard_articles_news')]
    public function addArticle(Request $request, SluggerInterface $slugger) : Response {
        //
        // Generate entity & form
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //
            // If image sent, upload this on server & add in db the unique name
            $iconUpload = $this->__generateIconID($slugger, $form->get('icon')->getData());

            if ($iconUpload != null) {
                $article->setIcon($iconUpload);
                $this->entityManager->persist($article);
            }

            //
            // If form is submitted and the values are checked good : save article in db & redirect to list overview
            $this->entityManager->persist($article);
            $this->entityManager->flush();

            $this->addFlash('sucess', 'L\'article a été sauvegardé avec succès.');
            return $this->redirectToRoute('dashboard_articles_list');
        }

        //
        // Render form layout
        return $this->render('dashboard/form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Écrire un article'
        ]);
    }

    /**
     * Edit article data
     * @param Request $request
     * @param SluggerInterface $slugger
     * @return Response
     */
    #[Route('/edit/{id}', name: 'dashboard_articles_edit')]
    public function editArticle(Request $request, SluggerInterface $slugger) : Response {
        $id = $request->get('id');

        if (empty($id)) {
            //
            // Fatal error : no ID passed in URL
            $this->addFlash('error', 'Une erreur interne est survenue.');
            return $this->redirectToRoute('dashboard_articles_list');
        }

        //
        // Get & check if the article exists
        $article = $this->entityManager->getRepository(Article::class)->find($id);

        if (!$article) {
            $this->addFlash('error', 'L\'article est introuvable.');
            return $this->redirectToRoute('dashboard_articles_list');
        }

        //
        // Make form & detect request (with submission & validation)
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //
            // If image sent, upload this on server & add in db the unique name
            $iconUpload = $this->__generateIconID($slugger, $form->get('icon')->getData());

            if ($iconUpload != null) {
                $article->setIcon($iconUpload);
                $this->entityManager->persist($article);
            }

            //
            // Success : push in DB & redirect to list
            $this->entityManager->flush();

            $this->addFlash('success', 'L\'article a été modifié avec succès.');
            return $this->redirectToRoute('dashboard_articles_list');
        }

        //
        // Render form layout
        return $this->render('dashboard/form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Modifier un article'
        ]);
    }

    /**
     * Upload image file and if OK return the unique name at string
     *
     * @param SluggerInterface $slugger
     * @param $image
     * @return string|null
     */
    private function __generateIconID(SluggerInterface $slugger, $image) : string|null {
        //
        // SImage sent
        if ($image) {

            //
            // Get file information & set unique name
            $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $slugger->slug($originalFilename);
            $newFilename = $safeFilename.'-'.uniqid().'.'.$image->guessExtension();

            try {
                //
                // Upload file on upload_directory defined in /config/service.yaml
                $image->move(
                    $this->getParameter('upload_directory'),
                    $newFilename
                );
            } catch (FileException $e) {
                throw new FileException($e);
            }

            return $newFilename;
        }

        return null;
    }

    /**
     * Destroy article
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/remove/{id}', name: 'dashboard_articles_delete')]
    public function removeArticle(Request $request) : Response {
        $id = $request->get('id');

        //
        // Check if ID is requested
        if (empty($id)) {
            $this->addFlash('error', 'Une erreur interne est survenue.');
            return $this->redirectToRoute('dashboard_articles_list');
        }

        $article = $this->entityManager->getRepository(Article::class)->find($id);

        //
        // Article not found
        if (!$article) {
            $this->addFlash('error', 'L\'article est introuvable.');
            return $this->redirectToRoute('dashboard_articles_list');
        }

        //
        // Remove
        $this->entityManager->remove($article);
        $this->entityManager->flush();

        //
        // Redirect to article lists
        $this->addFlash('success', 'L\'article a été supprimé correctement.');
        return $this->redirectToRoute('dashboard_articles_list');
    }

}