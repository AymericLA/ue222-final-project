<?php

namespace App\Controller\Dashboard;

use App\Entity\Category;
use App\Form\CategoryType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/dashboard/categories')]
class CategoriesManagementController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * View of list of categories
     * @return Response
     */
    #[Route('/list', name: 'dashboard_categories_list')]
    public function categoriesLists() : Response {
        $categories = $this->entityManager->getRepository(Category::class)->findAll();

        //
        // Set table header
        $tableHeader = [
            'N° identification',
            'Nom',
            'Actions'
        ];

        $table = [];

        //
        // Each category to set the data, with actions
        foreach ($categories as $category) {
            $table[] = [
                $category->getId(),
                $category->getName(),
                [
                    'Editer' => [
                        'class' => 'btn-primary',
                        'url' => 'dashboard_categories_edit',
                        'urlParameters' => [
                            'id' => $category->getId()
                        ]
                    ],
                    'Supprimer' => [
                        'class' => 'btn-danger',
                        'url' => 'dashboard_categories_remove',
                        'urlParameters' => [
                            'id' => $category->getId()
                        ]
                    ]
                ]
            ];
        }

        //
        // Extra action
        $extra = [
            'Nouvelle catégorie' => [
                'class' => 'btn-info',
                'url' => 'dashboard_categories_new'
            ]
        ];

        //
        // Render list layout
        return $this->render('dashboard/list.html.twig', [
            'table' => $table,
            'tableHeader' => $tableHeader,
            'extra' => $extra,
            'title' => 'Liste des catégories'
        ]);
    }

    /**
     * Create new category
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/new', name: 'dashboard_categories_new')]
    public function addCategory(Request $request) : Response {
        //
        // Generate entity & form
        $newCategory = new Category();
        $form = $this->createForm(CategoryType::class, $newCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //
            // Form requested & checked : detect if category name is already used
            $category = $this->entityManager->getRepository(Category::class)->findOneBy([
                'name' => $form->getData()->getName()
            ]);

            if (!empty($category)) {
                //
                // Name already used
                $this->addFlash('error', 'Une catégorie portant ce nom existe déjà !');
            } else {
                //
                // Save in DB
                $this->entityManager->persist($newCategory);
                $this->entityManager->flush();
                $this->addFlash('success', 'La catégorie a été sauvegardée.');
            }
        }

        //
        // Render form layout
        return $this->render('dashboard/form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Créer une nouvelle catégorie'
        ]);
    }

    /**
     * Delete category and associated articles
     * @param Request $request
     * @return Response
     */
    #[Route('/remove/{id}', name: 'dashboard_categories_remove')]
    public function removeCategory(Request $request) : Response {
        $id = $request->get('id');

        //
        // ID requested
        if (!empty($id)) {
            $category = $this->entityManager->getRepository(Category::class)->find($id);

            if (!empty($category)) {
                //
                // Category found
                $this->entityManager->remove($category);
                $this->entityManager->flush();
                $this->addFlash('success', 'La catégorie a été supprimée.');
            } else {
                $this->addFlash('error', 'La catégorie n\'existe pas.');
            }
        }

        return $this->redirectToRoute('dashboard_categories_list');
    }

    /**
     * Edit category form
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/edit/{id}', name: 'dashboard_categories_edit')]
    public function editCategory(Request $request) : Response {
        $id = $request->get('id');

        if (empty($id)) {
            //
            // ID not requested
            $this->addFlash('error', 'Une erreur est survenue.');
            return $this->redirectToRoute('dashboard_categories_list');
        }

        $category = $this->entityManager->getRepository(Category::class)->find($id);

        if (empty($category)) {
            //
            // ID not found in DB
            $this->addFlash('error', 'La catégorie n\'existe pas.');
            return $this->redirectToRoute('dashboard_categories_list');
        }

        //
        // Make form
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //
            // Form requested & checked : save data in DB
            $this->entityManager->flush();
            $this->addFlash('success', 'La catégorie a été modifiée.');
            return $this->redirectToRoute('dashboard_categories_list');
        }

        //
        // Render form layout
        return $this->render('dashboard/form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Modifier une catégorie'
        ]);
    }

}