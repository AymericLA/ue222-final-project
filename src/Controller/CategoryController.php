<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/category')]
class CategoryController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * List of categories
     *
     * @param CategoryRepository $categoryRepository
     * @return Response
     */
    #[Route('/', name: 'category_index', methods: ['GET'])]
    public function index(CategoryRepository $categoryRepository): Response
    {
        return $this->render('category/index.html.twig', [
            'categories' => $categoryRepository->findAll(),
        ]);
    }

    /**
     * Show one category data
     *
     * @param Category $category
     * @return Response
     */
    #[Route('/{id}', name: 'category_show', methods: ['GET'])]
    public function show(Request $request): Response
    {
        //
        // Check if ID is in request body
        if (!$request->get('id')) {
            $this->addFlash('error', 'Une erreur interne est survenue');
            return $this->redirectToRoute('category_index');
        }

        $category = $this->entityManager->getRepository(Category::class)->find($request->get('id'));

        //
        // Check if category exists
        if (!$category) {
            $this->addFlash('error', 'La catégorie demandée n\'existe pas');
            return $this->redirectToRoute('category_index');
        }

        //
        // Render category information
        return $this->render('category/show.html.twig', [
            'category' => $category,
        ]);
    }

    /**
     * Show last categories to be showed in nav dropdown
     *
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function getLastCategoriesForNavigation(EntityManagerInterface  $entityManager) {
        $categories = $entityManager->getRepository(Category::class)->getLastCategoriesForMenu();

        return $this->render('elements/menuCategories.html.twig', [
            'categories' => $categories
        ]);
    }
}
