<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\LoginType;
use App\Form\RegistrationType;
use App\Service\UserGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

#[Route('/account')]
class AuthController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Login with registered user
     *
     * @param Request $request
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    #[Route('/login', name: 'app_auth_login')]
    public function login(Request $request, AuthenticationUtils $authenticationUtils): Response
    {
        //
        // Get last error message from form
        $msg = $authenticationUtils->getLastAuthenticationError();
        $user = new User();

        //
        // Make form
        $form = $this->createForm(LoginType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //
            // Form submitted and valid :
            // -> check if username format is valid
            if (strlen($form->getData()->getUsername()) < 6) {
                $this->addFlash('error', 'Le nom d\'utilisateur doit comporter au moins 6 caractères ');
                return $this->redirectToRoute('app_auth_login');
            } else if (preg_match('/[^a-zA-Z\s]/', $form->getData()->getUsername()) === 1) {
                $this->addFlash('error', 'Le nom d\'utilisateur ne peut pas compter de caractères spéciaux ou de chiffres. ');
                return $this->redirectToRoute('app_auth_login');
            }

            //
            // Login accomplished
            $this->addFlash('success', 'Vous êtes maintenant connecté. ');
            return $this->redirectToRoute('app_auth_login');
        }

        //
        // Render form
        return $this->render('auth/login.html.twig', [
            'form' => $form->createView(),
            'message' => $msg
        ]);
    }

    /**
     * Save new user in DB
     *
     * @param Request $request
     * @param UserPasswordHasherInterface $hasher
     * @return Response
     */
    #[Route('/register', name: 'app_auth_register')]
    public function register(Request $request, UserPasswordHasherInterface $hasher): Response
    {
        //
        // Make form
        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //
            // Form submitted and valid : call UserGenerator service to make new user
            $userGenerator = new UserGenerator($this->entityManager, $hasher);
            $generationStatus = $userGenerator->createNewUser($form->getData(), $form->get('repeatPassword'));

            if ($generationStatus != null) {
                //
                // Error
                $this->addFlash($generationStatus[0], $generationStatus[1]);
            } else {
                //
                // Success
                $this->addFlash('success', 'Vous êtes maintenant inscrit. Vous pouvez vous connecter ');
            }

        }

        //
        // Render registration view
        return $this->render('auth/register.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
