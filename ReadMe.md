# Symfony - Blog : UE 222, CVTIC Limoges

**Aymeric Leger Achard**

La configuration du docker-compose a été modifiée
pour intégrer directement le serveur LAMP de CVTIC.

De plus le fichier :

**docker/sites-available/000-default.conf**

est automatiquement copier dans le chemin :

**/etc/apache2/sites-available/**

Ce qui a pour effet de limiter la configuration à faire.

---

## Installation

Exécuter le **docker-compose.yml** via Docker.

Après sa création, entrer dans le terminal du container "server".

Dans un premier temps il faudra mettre à jour le système :

````shell
apt update && apt upgrade
````

Dans un deuxième  temps nous allons installer NVM pour ensuite installer Node.js et NPM

````shell
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash

source /root/.bashrc

nvm install node
apt install npm
````

Ensuite nous allons mettre à jour le fichier Apache2 en copiant le fichier contenant
les bonnes configurations et recharger Apache2:

````shell
cp ./docker/sites-available/000-default.conf /etc/apache2/sites-available/

service apache2 reload
````

Maintenant nous pouvons initialiser le projet en lui-même :

````shell
composer self-update

composer install

npm install

npm run dev

php bin/console d:m:diff

php bin/console d:m:m
````

Si vous souhaitez installer les données de démonstration finale, utilisez la commande :

````shell

php bin/console doctrine:fixtures:load

````

---

## Se connecter

Avec les informations de connexion ci-dessous, vous pouvez 
vous connecter en tant qu'administrateur ou utilisateur (informations issues des données
de test que vous pouvez générer via "doctrine:fixtures:load").

| Nom d'utilisateur | Mot de passe | Role           |
|-------------------|--------------|----------------|
| administrator     | HelloAdmin1& | Administrateur |
| administrator_2   | HelloAdmin2& | Administrateur |
| stduser           | HelloUser1&  | Utilisateur    |
| stduser_2         | HelloUser2&  | Utilisateur    |